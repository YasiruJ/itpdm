<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::get('/testform', function () {
    return view('testform');
});


Route::get('/testform/{email}', 'TestController@ViewTest');
Route::get('/email', 'TestController@mailsend');

Route::post('personalitytest/testform', 'TestController@PostView');

Route::post('personalitytest/results', 'TestController@TestPerson');

Route::get('results/{jobrole}', 'TestController@ViewResults');

//Route::get('/', function () {
//    return view('results');
//});

