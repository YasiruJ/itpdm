<!DOCTYPE html>
<html>
<head>
	<title>WhizChain Personality Test</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    
    <style>
             .contain {
            width: 100%;
            height: 100vh;
            background-image: url('../7.jpeg');
            background-size: cover;
            background-position: center;
        }
    </style>

</head>
<body>
<div class="contain">
    <nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark" style="background-color: #212529!important;opacity: 0.85">
        <a class="navbar-brand ptext" href="#" style="color: greenyellow;font-family: cursive;font-size: 55px;">Personality Management System</a>
    </nav>
        <div class="contain">
            <div class="row box">
                    <div class="col-md-3"></div>
                    <div class="card bg-light mb-3 col-md-6" style="top: 200px;">
                    <div class="card-header">Your personality is {{$jobrole}}</div>
                    <div class="card-body">
                        <h5 class="card-title">Job Suggetions</h5>
                        <p class="card-text">
                            @foreach($jobs as $job)
                                <label>{{$job->role}}</label> <br>
                            @endforeach
                        </p>
                    </div>
                    </div>
                    <div class="col-md-3"></div>
            </div>
        </div>
</div>
</body>
</html>
