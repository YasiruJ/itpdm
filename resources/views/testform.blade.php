<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>WhizChain Personality Test</title>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="{{'/css/slideform.css'}}">

    <style>


    form {
        opacity: 0;
        transition: all .3s ease;
    }

    form.slideform-form {
        opacity: 1;
    }
    /*
    {{--html, body {--}}
    {{--    background-image:url({{'images/cork-board.png'}}); --}}
    {{--    background:cornsilk;--}}
    {{--    height: 100%;--}}
    {{--    width: 100%;--}}
    {{--    padding: 0;--}}
    {{--    margin: 0;--}}
    {{--    overflow: hidden;--}}
    {{--    position: fixed;--}}
    {{--    top: 0;--}}
    {{--    bottom: 0;--}}
    {{--    left: 0;--}}
    {{--    right: 0;--}}
    {{--}--}}
    */
    #testform {

        background-image:url({{'../images/email-pattern.png'}});
        height: 100%;
        width: 100%;
        padding: 0;
        margin: 0;
        overflow: hidden;
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
    }

    body, h1, h2, h3, p, button, input, select, textarea {
        font-family: 'Raleway', Arial, sans-serif;
    }

    input {
        font-size: 16px;
    }

    label {
        text-transform: uppercase;
        /*display: block;*/
        font-size: 14px;
        margin-bottom: 10px;
        letter-spacing: .5px;
        font-weight: 600;
        font-family: Raleway;
    }

    h1 {
        letter-spacing: .5px;
    }

    h2, h3 {
        margin-top: 0;
    }

    h2 {
        font-size: 28px;
        margin: 0 0 15px;
    }

    h3 {
        font-size: 20px;
        line-height: 1.4em;
        font-weight: bold;
        margin-bottom: 20px;
        margin-top: 20px;
    }

    h4 {
        font-size: 16px;
        font-weight: bold;
        margin: 20px 0 10px;
    }

    p {
        font-size: 16px;
        line-height: 1.6em;
        margin-bottom: 15px;
    }

    a {
        -webkit-tap-highlight-color: transparent;
    }


    .gist .gist-file {
        margin-top: 40px;
    }

    .gist .gist-meta {
        display: none !important;
    }

    .ctn-preloader {
        align-items: center;
        -webkit-align-items: center;
        display: flex;
        display: -ms-flexbox;
        height: 100%;
        justify-content: center;
        -webkit-justify-content: center;
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        z-index: 100;
        background: #fff
    }
    </style>

</head>

<body>

    <section>
        <div id="preloader">
            <div id="ctn-preloader" class="ctn-preloader">
                <div class="animation-preloader">
                    <div class="txt-loading">
                        <div class="cd-slideshow-wrapper" id="loading" style="">
                            <img id="loading-image" src="{{URL('images/loader.gif')}}" style=" margin-bottom: 50px; height: 100px; width: 100px;" alt="Loading..." />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <form id="testform">
        <div class="col-md-12" style="margin-top: 80px;"><h1 style="text-align: center;">Which one do you prefer ?</h1></div>
        <div class="slideform-slide">
            <div class="slideform-group">
{{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q1" value="1"><span>expend energy, enjoy groups</span></label>
                    <label for=""><input type="radio" name="q1" value="2"><span>conserve energy, enjoy one-on-one</span></label>
                </div>

            </div>
        </div>


        <div class="slideform-slide">
            <div class="slideform-group">
{{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q2" value="1"><span>interpret literally</span></label>
                    <label for=""><input type="radio" name="q2" value="2"><span>look for meaning and possibilities</span></label>
                    <div id="show"></div>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
{{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q3" value="1"><span>logical, thinking, questioning</span></label>
                    <label for=""><input type="radio" name="q3" value="2"><span>empathetic, feeling, accommodating</span></label>
                </div>
            </div>
        </div>


        <div class="slideform-slide">
            <div class="slideform-group">
{{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q4" value="1"><span>organized, orderly</span></label>
                    <label for=""><input type="radio" name="q4" value="2"><span>flexible, adaptable</span></label>
                </div>
            </div>
        </div>



        <div class="slideform-slide">
            <div class="slideform-group">
{{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q5" value="1"><span>more outgoing, think out loud</span></label>
                    <label for=""><input type="radio" name="q5" value="2"><span>more reserved, think to yourself</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q6" value="1"><span>practical, realistic, experiential</span></label>
                    <label for=""><input type="radio" name="q6" value="2"><span>imaginative, innovative, theoretical</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q7" value="1"><span>candid, straight forward, frank</span></label>
                    <label for=""><input type="radio" name="q7" value="2"><span>tactful, kind, encouraging</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q8" value="1"><span>plan, schedule</span></label>
                    <label for=""><input type="radio" name="q8" value="2"><span>unplanned, spontaneous</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q9" value="1"><span>seek many tasks, public activities, interaction with others</span></label>
                    <label for=""><input type="radio" name="q9" value="2"><span>seek private, solitary activities with quiet to concentrate</span></label>
                </div>
            </div>
        </div>


        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q10" value="1"><span>standard, usual, conventional</span></label>
                    <label for=""><input type="radio" name="q10" value="2"><span>different, novel, unique</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q11" value="1"><span>firm, tend to criticize, hold the line</span></label>
                    <label for=""><input type="radio" name="q11" value="2"><span>gentle, tend to appreciate, conciliate</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q12" value="1"><span>regulated, structured</span></label>
                    <label for=""><input type="radio" name="q12" value="2"><span>easygoing, “live” and “let live”</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q13" value="1"><span>external, communicative, express yourself</span></label>
                    <label for=""><input type="radio" name="q13" value="2"><span>internal, reticent, keep to yourself</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}
                <div class="row options options-list">
                    <label for=""><input type="radio" name="q14" value="1"><span>focus on here-and-now</span></label>
                    <label for=""><input type="radio" name="q14" value="2"><span>look to the future, global perspective, “big picture”</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q15" value="1"><span>tough-minded, just</span></label>
                    <label for=""><input type="radio" name="q15" value="2"><span>tender-hearted, merciful</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q16" value="1"><span>preparation, plan ahead</span></label>
                    <label for=""><input type="radio" name="q16" value="2"><span>go with the flow, adapt as you go</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q17" value="1"><span>active, initiate</span></label>
                    <label for=""><input type="radio" name="q17" value="2"><span>reflective, deliberate</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q18" value="1"><span>facts, things, “what is”</span></label>
                    <label for=""><input type="radio" name="q18" value="2"><span>ideas, dreams, “what could be,” philosophical</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q19" value="1"><span>matter of fact, issue-oriented</span></label>
                    <label for=""><input type="radio" name="q19" value="2"><span>sensitive, people-oriented, compassionate</span></label>
                </div>
            </div>
        </div>

        <div class="slideform-slide">
            <div class="slideform-group">
                {{--                <h1>Hello Slideforms</h1>--}}

                <div class="row options options-list">
                    <label for=""><input type="radio" name="q20" value="1"><span>control, govern</span></label>
                    <label for=""><input type="radio" name="q20" value="2"><span>latitude, freedom</span></label>
                </div>
            </div>
        </div>



        <footer class="slideform-footer">
            <div class="slideform-progress-bar">
                <span></span>
            </div>

            <div class="buttons">
                <button class="slideform-btn slideform-btn-next"><i class="icon-chevron-down"></i></button>
                <button class="slideform-btn slideform-btn-prev"><i class="icon-chevron-up"></i></button>
            </div>
        </footer>
        <input type="hidden" id="nameinput" name="nameinput" value="{{$email ?? ''}}">
    </form>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
    <script
      src="https://code.jquery.com/jquery-3.3.1.min.js"
      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
      crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
    <script src="{{URL('js/slideform.js')}}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        ( function ($) {
            jQuery.validator.addMethod("equals", function(value, element, param) {
              return this.optional(element) || value === param;
            });

            $(document).ready( function () {
                $("#preloader").hide();

                var $form = $('#testform');

                $form.slideform({
                    validate: {
                        rules: {
                            q1: {
                                required:true,
                            },
                            q2: {
                                required:true,
                            },
                            q3: {
                                required:true,
                            },
                            q4: {
                                required:true,
                            },
                            q5: {
                                required:true,
                            },
                            q6: {
                                required:true,
                            },
                            q7: {
                                required:true,
                            },
                            q8: {
                                required:true,
                            },
                            q9: {
                                required:true,
                            },
                            q10: {
                                required:true,
                            },
                            q11: {
                                required:true,
                            },
                            q12: {
                                required:true,
                            },
                            q13: {
                                required:true,
                            },
                            q14: {
                                required:true,
                            },
                            q15: {
                                required:true,
                            },
                            q16: {
                                required:true,
                            },
                            q17: {
                                required:true,
                            },
                            q18: {
                                required:true,
                            },
                            q19: {
                                required:true,
                            },
                            q20: {
                                required:true,
                            }
                        },
                        messages: {
                            q1: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q2: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q3: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q4: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q5: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q6: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q7: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q8: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q9: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q10: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q11: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q12: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q13: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q14: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q15: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q16: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q17: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q18: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q19: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            },
                            q20: {
                                required: 'Please select an option',
                                equals: 'You must select valid!'
                            }
                        }
                    },
                    submit: function (event, form) {
                        $("#testform").hide();
                        $("#preloader").show();
                        $form.trigger('goForward');

                        if($('#testform').valid()) {
                            event.preventDefault();
                            var name = $("#nameinput").val();

                            console.log('submit');
                            console.log(name);
                            $.ajax({
                                url: "{{url('/personalitytest/results')}}",
                                method: "POST",
                                data: $('#testform').serialize(),
                                success: function(response) {
                                    $("#loader").hide();
                                    console.log(response);
                                    // $id = response;
                                    window.location.href = "{{url('results/jobrole=')}}"+response;
                                }
                            });

                        }

                    }
                });
            });
        }(jQuery))
    </script>
</body>
</html>
