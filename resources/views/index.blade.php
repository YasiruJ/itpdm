<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>WhizChain Personality Test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/confirm_popup/jquery-confirm.css') }}"/>
    <style>
        .contain {
            width: 100%;
            height: 100vh;
            background-image: url('7.jpeg');
            background-size: cover;
            background-position: center;
        }
        .click {

            margin: 0;
            position: absolute;
            top: 50%;
            left: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
            -ms-transform: translateX(-50%);
            transform: translateX(-50%);
            /*width:500px;*/
            /*position:absolute;*/
            /*left:50%;*/
            /*top:50%;*/
            /*margin: -75px 0 0 -160px;*/


        }

        .clickme {

            background-color:#1f1f2e!important;
            padding: 30px;
            box-shadow: 0 0 1px #ccc;
            -webkit-transition: all 0.5s ease-in-out;

        }
        .clickme:hover{
           -webkit-transform: scale(1.1);
            background: #31708f;
        }


        .bg-modal {
            background-color: rgba(0, 0, 0, 0.8);
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            display: none;
            justify-content: center;
            align-items: center;
        }

        .modal-contents {
            height: 580px;
            width: 500px;
            background-color: white;
            /*text-align: center;*/
            padding: 20px;
            position: relative;
            border-radius: 4px;
        }

        input {
            margin: 15px auto;
            display: block;
            width: 50%;
            padding: 8px;
            border: 1px solid gray;
        }

        .btntry {
            background-color: #4CAF50;
            color: white;
            padding: 16px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            width: 100%;
            margin-bottom: 10px;
            opacity: 0.8;
        }
        .closer {
            background-color: red;
            color: white;
            padding: 16px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            width: 100%;
            margin-bottom: 10px;
            opacity: 0.8;
        }

        .labeltext {
            margin-bottom: 0 !important;
        }

        .inputtext {
            margin: 0 !important;
        }
        @media only screen and (max-width: 850px) {
            .ptext {
                font-size: xx-large !important;
            }
        }

            @media only screen and (max-width: 500px) {
                .ptext {
                    font-size: larger !important;
                }

            }


    </style>
</head>
<body>
<div class="contain">
    <nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark" style="background-color: #212529!important;opacity: 0.85">
        <a class="navbar-brand ptext" href="#" style="color: greenyellow;font-family: cursive;font-size: 55px;">Personality Management System</a>
    </nav>


    <div class="col-md-4 col-xs-4 click">
        <button type="button" id="okbutton" class="btn btn-secondary clickme" style="font-size:x-large;border-radius: 20px;height: 100%;width: 100%;color: greenyellow;opacity: 0.8;">Test Your Personality</button>
    </div>





    <div class="bg-modal">
        <div class="modal-contents">

            <div><h3>Get Your Personality Report to Your Email</h3></div>


            <form action="#" id="testform" method="post" onsubmit="false">
                {{csrf_field()}}

                <div class="form-group">
                    <label class="form-control-label labeltext" for="input-username" >First Name</label>
                    <input type="text" id="first_name" name="first_name"  class="form-control form-control-alternative inputtext" placeholder="First name" required>
                </div>
                <div class="form-group">
                    <label class="form-control-label labeltext" for="input-username">Last Name</label>
                    <input type="text" id="last_name" name="last_name" class="form-control form-control-alternative inputtext" placeholder="Last Name" required>
                </div>
                <div class="form-group">
                    <label class="form-control-label labeltext" for="input-username">E-mail</label>
                    <input type="text" id="email" name="email" class="form-control form-control-alternative inputtext" placeholder="E-mail" required>
                </div>
                <div class="form-group">
                    <label class="form-control-label labeltext" for="input-username">Date Of Birth</label>
                    <input type="date" id="dob" name="dob" class="form-control form-control-alternative inputtext" required>
                </div>
                <input type="submit" id="btntry" class="button btntry" value="Try Now">
            </form>
            <div class="text-center" style="margin-bottom: 5px;"><a href="{{URL('/testform')}}">No,I just want to check</a></div>
            <button type="button" class="closer">Close</button>

        </div>
    </div>



</div>


<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    document.getElementById('okbutton').addEventListener("click", function() {
        document.querySelector('.bg-modal').style.display = "flex";
    });

    document.querySelector('.closer').addEventListener("click", function() {
        document.querySelector('.bg-modal').style.display = "none";
    });

    $("#testform").on('submit', function(event) {
        event.preventDefault();
        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();
        var email = $("#email").val();
        var dob = $("#dob").val();


        if(!ValidateEmail(email)) {
            showError("Please enter valid email");
            return false;
        }
        $.ajax({
            url: "{{url('/personalitytest/testform')}}",
            method: "POST",
            data: "first_name="+first_name+"&last_name="+last_name+"&email="+email+"&dob="+dob,
            dataType: 'JSON',
            success: function(response) {
                    console.log(response);
                window.location.href = "{{url('/testform/email=')}}"+email;
                {{--$(location).attr('href',"{{url('/testform')}}")--}}
            }

        });
    });

    function ValidateEmail(mail) {
        var pattern = new RegExp (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
        return !!pattern.test(mail);
    }


    function showError(message) {
        $.confirm({
            icon: 'fa fa-exclamation-triangle',
            theme: 'modern',
            animation: 'left',
            type: 'red',
            title: 'Error!',
            content: message,
            buttons: {
                close: function () {
                }
            }
        });
    }
</script>
<script type="text/javascript" src="{{ asset('css/confirm_popup/jquery-confirm.js') }}"></script>
</body>
</html>
