<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examiner extends Model
{
    protected $table = 'examiner';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
