<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supporter extends Model
{
    protected $table = 'supporter';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
