<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Confidant extends Model
{
    protected $table = 'confidant';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
