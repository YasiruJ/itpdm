<?php

namespace App\Http\Controllers;
use App\Advocate;
use App\Artist;
use App\Chief;
use App\Confidant;
use App\Craftsman;
use App\Defender;
use App\Dreamer;
use App\Engineer;
use App\Entertainer;
use App\Examiner;
use App\Mentor;
use App\Originator;
use App\Overseer;
use App\Persuader;
use App\Strategist;
use App\Supporter;
use Illuminate\Support\Facades\View;
use App\User;
use Illuminate\Http\Request;
use \App\Mail\SendMail;
use Illuminate\Support\Facades\Storage;
use Barryvdh\DomPDF\Facade as PDF;
class TestController extends Controller
{


    public function ViewHome()
    {
        return view('index');
    }

    public function PostView(Request $request)
    {

//        dump($first_name);
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $email = $request->input('email');
        $dob = $request->input('dob');

        $users = new User();

        $users->first_name = $first_name;
        $users->last_name = $last_name;
        $users->email = $email;
        $users->dob = $dob;

        $users->save();

        $res['success'] = true;
        $res['message'] = $first_name;
        return response($res);



    }

    public function ViewTest($emails)
    {

        list($title, $email) = explode('=', $emails);

        return view('testform',['email'=>$email]);
    }

    public function TestPerson(Request $request)
    {

        $email = $request->input('nameinput');

        $first_a_total = 0;
        $first_b_total = 0;

        $second_a_total = 0;
        $second_b_total = 0;

        $third_a_total = 0;
        $third_b_total = 0;

        $fourth_a_total = 0;
        $fourth_b_total = 0;

        $first_set_letter = '';
        $second_set_letter = '';
        $third_set_letter = '';
        $fourth_set_letter = '';
        $final_word = '';
        $job_role = '';

        $data = $request->all();

        $result = [];

        foreach ($data as $res) {

            $result[] = $res;

        }

        $first_set = array();
        $second_set = array();
        $third_set = array();
        $fourth_set = array();
        array_push($first_set , $result[0] ,$result[4] ,$result[8] ,$result[12] ,$result[16] );
        array_push($second_set , $result[1] ,$result[5] ,$result[9] ,$result[13] ,$result[17] );
        array_push($third_set , $result[2] ,$result[6] ,$result[10] ,$result[14] ,$result[18] );
        array_push($fourth_set , $result[3] ,$result[7] ,$result[11] ,$result[15] ,$result[19] );


//        This is for the first set

        foreach ($first_set as $first) {

            if($first == 1) {
                $first_a_total = $first_a_total +1;
            }
            else {
                $first_b_total = $first_b_total + 1;
            }

        }

        if ($first_a_total>$first_b_total) {

            $first_set_letter = 'E';
        }
        else {

            $first_set_letter = 'I';
        }

//=========================================

//        This is for the second set

        foreach ($second_set as $second) {

            if($second == 1) {
                $second_a_total = $second_a_total +1;
            }
            else {
                $second_b_total = $second_b_total + 1;
            }

        }

        if ($second_a_total>$second_b_total) {

            $second_set_letter = 'S';
        }
        else {

            $second_set_letter = 'N';
        }

//=========================================


//        This is for the third set

        foreach ($third_set as $third) {

            if($third == 1) {
                $third_a_total = $third_a_total +1;
            }
            else {
                $third_b_total = $third_b_total + 1;
            }

        }

        if ($third_a_total>$third_b_total) {

            $third_set_letter = 'T';
        }
        else {

            $third_set_letter = 'F';
        }

//=========================================

//        This is for the fourth set

        foreach ($fourth_set as $fourth) {

            if($fourth == 1) {
                $fourth_a_total = $fourth_a_total +1;
            }
            else {
                $fourth_b_total = $fourth_b_total + 1;
            }

        }

        if ($fourth_a_total>$fourth_b_total) {

            $fourth_set_letter = 'J';
        }
        else {

            $fourth_set_letter = 'P';
        }

//=========================================

        $final_word = $first_set_letter.$second_set_letter.$third_set_letter.$fourth_set_letter;


        if($final_word == 'ESTJ') {
            $job_role = 'Overseer';
        }
        else if($final_word == 'ESFJ') {
            $job_role = 'Supporter';
        }
        else if($final_word == 'ISTJ') {
            $job_role = 'Examiner';
        }
        else if($final_word == 'ISFJ') {
            $job_role = 'Examiner';
        }
        else if($final_word == 'ESTP') {
            $job_role = 'Persuader';
        }
        else if($final_word == 'ESFP') {
            $job_role = 'Entertainer';
        }
        else if($final_word == 'ISTP') {
            $job_role = 'Craftsman';
        }
        else if($final_word == 'ISFP') {
            $job_role = 'Artist';
        }
        else if($final_word == 'ENTJ') {
            $job_role = 'Chief';
        }
        else if($final_word == 'ENTP') {
            $job_role = 'Orginator';
        }
        else if($final_word == 'INTJ') {
            $job_role = 'Strategist';
        }
        else if($final_word == 'INTP') {
            $job_role = 'Engineer';
        }
        else if($final_word == 'ENFJ') {
            $job_role = 'Mentor';
        }
        else if($final_word == 'ENFP') {
            $job_role = 'Advocate';
        }
        else if($final_word == 'INFJ') {
            $job_role = 'Confidant';
        }
        else if($final_word == 'INFP') {
            $job_role = 'Dreamer';
        }

//        methanin if ekak danna

        if ($email!== null) {


            $details = [
                'title' => $job_role,
                'body' => 'Body: This is for testing email using smtp'

            ];

            \Mail::to($email)->send(new SendMail($details));
//        return view('results',['jobrole'=>$job_role,'jobs'=>$jobs]);
            return response($job_role);
        }

//        send result to ajax response

            return response($job_role);




    }



    public function ViewResults($jobrole) {



        list($title, $result) = explode('=', $jobrole);




        if($result === "Overseer") {

            $jobs = Overseer::all();

        }
        else if ($result === "Supporter") {

            $jobs = Supporter::all();

        }
        else if ($result === "Examiner") {

            $jobs = Examiner::all();
        }
        else if ($result === "Defender") {

            $jobs = Defender::all();
        }
        else if ($result === "Persuader") {

            $jobs = Persuader::all();
        }
        else if ($result === "Entertainer") {

            $jobs = Entertainer::all();
        }
        else if ($result === "Craftsman") {

            $jobs = Craftsman::all();
        }
        else if ($result === "Artist") {

            $jobs = Artist::all();
        }
        else if ($result === "Chief") {

            $jobs = Chief::all();
        }
        else if ($result === "Originator") {

            $jobs = Originator::all();
        }
        else if ($result === "Strategist") {

            $jobs = Strategist::all();
        }
        else if ($result === "Engineer") {

            $jobs = Engineer::all();
        }
        else if ($result === "Mentor") {

            $jobs = Mentor::all();
        }
        else if ($result === "Advocate") {

            $jobs = Advocate::all();
        }
        else if ($result === "Confidant") {

            $jobs = Confidant::all();
        }
        else if ($result === "Dreamer") {

            $jobs = Dreamer::all();
        }


        return view('results',['jobrole'=>$result,'jobs'=>$jobs]);


    }


}
