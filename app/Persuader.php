<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persuader extends Model
{
    protected $table = 'persuader';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
