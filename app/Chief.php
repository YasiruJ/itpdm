<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chief extends Model
{
    protected $table = 'chief';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
