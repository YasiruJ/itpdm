<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dreamer extends Model
{
    protected $table = 'dreamer';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
