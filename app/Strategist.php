<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Strategist extends Model
{
    protected $table = 'strategist';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
