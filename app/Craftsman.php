<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Craftsman extends Model
{
    protected $table = 'craftsman';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
