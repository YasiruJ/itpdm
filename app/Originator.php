<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Originator extends Model
{
    protected $table = 'originator';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
