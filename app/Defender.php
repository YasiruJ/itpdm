<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Defender extends Model
{
    protected $table = 'defender';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
