<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    protected $primarykey = 'id';
    protected $fillable = ['id,first_name,last_name,email,dob'];
    public $timestamps = false;

}
