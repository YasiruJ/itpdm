<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advocate extends Model
{
    protected $table = 'advocate';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
