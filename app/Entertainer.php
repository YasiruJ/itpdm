<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entertainer extends Model
{
    protected $table = 'entertainer';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
