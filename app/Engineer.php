<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engineer extends Model
{
    protected $table = 'engineer';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
