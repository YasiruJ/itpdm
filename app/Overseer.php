<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Overseer extends Model
{
    protected $table = 'overseer';
    protected $primarykey = 'id';
    protected $fillable = ['id,role'];
    public $timestamps = false;
}
